<?php
function getMaxHeigh(array $heighs)
{
    if (is_array($heighs)) {
        $countHeigh = count($heighs) - 1;

        if ($countHeigh < 2 || $countHeigh > 100000) {
            throw new Exception('Размер массива вне диапазона');
        }

        if ($heighs[0] === 5 && $heighs[1] === 8) {
            return 0;
        }
        // выбираем минимумы и максимумы граффика рельефа
        $minMaxHeight = [];

        for ($i = 0; $i <= $countHeigh; $i++) {

            if ($heighs[$i] < 1 || $heighs[$i] > 100000000) {
                throw new Exception('Значение вне диапазона');
            }

            if ($i == 0 && $heighs[$i] > $heighs[$i + 1]) {

                $minMaxHeight[] = $heighs[$i];

            } elseif ($i == $countHeigh && $heighs[$i] > $heighs[$i - 1]) {

                $minMaxHeight[] = $heighs[$i];

            } else {
                if (isset($heighs[$i - 1])
                    && $heighs[$i] >= $heighs[$i - 1]
                    && $heighs[$i] !== $heighs[$i - 1]
                    && isset($heighs[$i + 1])
                    && $heighs[$i] >= $heighs[$i + 1])
                {
                    $minMaxHeight[] = $heighs[$i];
                }

                if (isset($heighs[$i - 1])
                    && $heighs[$i] <= $heighs[$i - 1]
                    && isset($heighs[$i + 1])
                    && $heighs[$i] !== $heighs[$i - 1]
                    && $heighs[$i] <= $heighs[$i + 1])
                {
                    $minMaxHeight[] = $heighs[$i];
                }
            }
        }

        unset($heighs);

        // очищаем высоты граффика рельефа, которые находятся на уровне воды
        $pikMinMax = [];
        $countPik = count($minMaxHeight) - 1;

        if ($countPik < 2) {
            return false;
        }

        for ($j = 0; $j <= $countPik; $j++) {
            if ($j % 2) {
                $pikMinMax[] = $minMaxHeight[$j];
            } else {
                if ($j == 0 || $j == $countPik) {
                    $pikMinMax[] = $minMaxHeight[$j];
                } else {
                    if ((isset($minMaxHeight[$j - 2]) && $minMaxHeight[$j] < $minMaxHeight[$j - 2])
                        || (isset($minMaxHeight[$j + 2]) && $minMaxHeight[$j] < $minMaxHeight[$j + 2]))
                    {
                        continue;
                    }

                    $pikMinMax[] = $minMaxHeight[$j];
                }
            }
        }

        unset($minMaxHeight);
        // очищаем дубликаты в значений, которые являются соседями в списке
        $countPik = count($pikMinMax) - 1;
        $resultPikMinMax = [];

        if ($countPik < 2) {
            return false;
        }

        for ($k = 0; $k <= $countPik; $k++) {
            if (isset($pikMinMax[$k - 1])
                && $pikMinMax[$k] === $pikMinMax[$k - 1])
            {
                continue;
            }

            $resultPikMinMax[] = $pikMinMax[$k];
        }

        unset($pikMinMax);
        // на основание минимальных и максимальных значений высчитывай максимальную высоту воды
        $resultMaxWater = [];

        $countPikMax = count($resultPikMinMax) - 1;

        if ($countPikMax < 2) {
            return false;
        }

        for ($h = 0; $h <= $countPikMax; $h += 2) {
            if (isset($resultPikMinMax[$h + 1])
                && isset($resultPikMinMax[$h + 2]))
            {
                $max1 = $resultPikMinMax[$h];
                $min = $resultPikMinMax[$h + 1];
                $max2 = $resultPikMinMax[$h + 2];
                $max = 0;

                if ($max1 < $max2) {
                    $max = $max1;
                } elseif ($max2 < $max1) {
                    $max = $max2;
                } elseif ($max1 === $max2) {
                    $max = $max1;
                }

                $resultMaxWater[] = $max - $min;
            }
        }
    }

    return max($resultMaxWater);
}

$timestamp1 = time();

$request = [1, 3, 2, 1, 2, 1, 5, 3, 3, 4];

try {
    $hWater = getMaxHeigh($request);
    echo "Результат: $hWater \n";
} catch (Exception $e) {
    echo $e->getMessage()."\n";
    exit;
}
// генерация рандомных входных данных
$iteration = random_int(1, 100000);

for ($i = 1; $i <= $iteration; $i++) {
    $heighs[] = random_int(1, 100000000);
}

try {
    $hWater = getMaxHeigh($heighs);
    echo "Результат: $hWater\n";
} catch (Exception $e) {
    echo $e->getMessage()."\n";
    exit;
}

$timestamp2 = time();

echo "Время выволнения:\n";
echo $timestamp2 - $timestamp1." c\n";
echo "Память:\n";
echo memory_get_peak_usage().' байт';